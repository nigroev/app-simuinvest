package br.com.itau.appsimuinvest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.itau.appsimuinvest.dto.ClienteDTO;
import br.com.itau.appsimuinvest.error.ClienteNotFoundException;
import br.com.itau.appsimuinvest.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService service;
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ClienteDTO create(@RequestBody @Valid ClienteDTO clienteEntry) {
        return service.create(clienteEntry);
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    ClienteDTO delete(@PathVariable("id") Integer id) {
        return service.delete(id);
    }
 
    @RequestMapping(method = RequestMethod.GET)
    List<ClienteDTO> findAll() {
        return service.findAll();
    }
 
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    ClienteDTO findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }
 
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    ClienteDTO update(@RequestBody @Valid ClienteDTO clienteEntry) {
        return service.update(clienteEntry);
    }
 
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<?> handleNotFound(ClienteNotFoundException ex){
		 return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	 }
	
}
