package br.com.itau.appsimuinvest.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.itau.appsimuinvest.dto.ContratoDTO;
import br.com.itau.appsimuinvest.error.ContratoNotFoundException;
import br.com.itau.appsimuinvest.service.ContratoService;

@RestController
@RequestMapping("/contrato")
public class ContratoController {

	@Autowired
	private ContratoService service;
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	ContratoDTO create(@RequestBody @Valid ContratoDTO contratoEntry){
		return service.create(contratoEntry);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	ContratoDTO delete(@PathVariable("id") Integer id) {
		return service.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	List<ContratoDTO> findAll() {
		return service.findAll();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ContratoDTO findById(@PathVariable("id") Integer id) {
		return service.findById(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	ContratoDTO update(@RequestBody @Valid ContratoDTO contratoEntry) {
		return service.update(contratoEntry);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<?> handleNotFound(ContratoNotFoundException ex){
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	}
}
