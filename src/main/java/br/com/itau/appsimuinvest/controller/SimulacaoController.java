package br.com.itau.appsimuinvest.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.appsimuinvest.dto.OfertaDTO;
import br.com.itau.appsimuinvest.dto.SimulacaoDTO;
import br.com.itau.appsimuinvest.service.SimulacaoService;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

	@Autowired
	private SimulacaoService service;
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	OfertaDTO create(@RequestBody @Valid SimulacaoDTO simulacaoEntry) {
        return service.simular(simulacaoEntry);
    }
}
