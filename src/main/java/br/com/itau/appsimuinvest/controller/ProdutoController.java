package br.com.itau.appsimuinvest.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.itau.appsimuinvest.dto.ProdutoDTO;
import br.com.itau.appsimuinvest.error.ProdutoNotFoundException;
import br.com.itau.appsimuinvest.service.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoService service;
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ProdutoDTO create(@RequestBody @Valid ProdutoDTO clienteEntry) {
        return service.create(clienteEntry);
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    ProdutoDTO delete(@PathVariable("id") Integer id) {
        return service.delete(id);
    }
 
    @RequestMapping(method = RequestMethod.GET)
    List<ProdutoDTO> findAll() {
        return service.findAll();
    }
 
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    ProdutoDTO findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }
 
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    ProdutoDTO update(@RequestBody @Valid ProdutoDTO clienteEntry) {
        return service.update(clienteEntry);
    }
 
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound(ProdutoNotFoundException ex) {
    }
}
