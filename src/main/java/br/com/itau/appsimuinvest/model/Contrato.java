package br.com.itau.appsimuinvest.model;

import static util.PreCondition.notEmpty;
import static util.PreCondition.notNull;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
public final class Contrato {
	
	public static final BigDecimal MIN_VALUE_VALOR_INVESTIDO = BigDecimal.valueOf(50.00);

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Integer id;
	
	@OneToOne
	private Cliente cliente;
	
	@ManyToOne
	private Produto produto;
	
	private Date dataContratacao;
	
	private BigDecimal capitalInvestido;
	
	public Contrato(){}
	
	public Contrato(Builder builder){
		super();
		this.cliente = builder.cliente;
		this.produto = builder.produto;
		this.dataContratacao = builder.dataContratacao;
		this.capitalInvestido = builder.capitalInvestido;
	}
	
	public void update(Cliente cliente, Produto produto, Date dataContratacao, BigDecimal capitalInvestido){
		
		checkObject(cliente, produto, dataContratacao, capitalInvestido);
		
		this.cliente = cliente;
		this.produto = produto;
		this.dataContratacao = dataContratacao;
		this.capitalInvestido = capitalInvestido;
	}
	
	public static Builder getBuilder(){
		 return new Builder();
	}
	
	 @NoArgsConstructor
	 public static class Builder {
	
		 private Cliente cliente;
		 private Produto produto;
		 private Date dataContratacao;
		 private BigDecimal capitalInvestido;
		 
		 public Builder cliente(Cliente cliente){
			 this.cliente = cliente;
			 return this;
		 }
		 
		 public Builder produto(Produto produto){
			 this.produto = produto;
			 return this;
		 }
		 
		 public Builder dataContratacao(Date dataContratacao){
			 this.dataContratacao = dataContratacao;
			 return this;
		 }
		 
		 public Builder capitalInvestido(BigDecimal capitalInvestido){
			 this.capitalInvestido = capitalInvestido;
			 return this;
		 }
		 
		 public Contrato build(){
			 Contrato build = new Contrato(this);
			 build.checkObject(build.getCliente(), build.getProduto(), build.getDataContratacao(), build.getCapitalInvestido());
			 return build;
		 }
	 } 
	 
	 private void checkObject(Cliente cliente, Produto produto, Date dataContratacao, BigDecimal capitalInvestido){

		 notNull(cliente, "Cliente não pode ser nulo");
		 notNull(produto, "Produto não pode ser nulo");
		 notNull(dataContratacao, "Data de contratação não pode ser nula");
		 notEmpty(dataContratacao.toString(), "Data de contratação não pode estar vazia");
		 notNull(capitalInvestido, "capital investido não pode ser nulo");
	 }
	 
}
