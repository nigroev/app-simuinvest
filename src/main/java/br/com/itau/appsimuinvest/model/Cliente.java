package br.com.itau.appsimuinvest.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import static util.PreCondition.isTrue;
import static util.PreCondition.notEmpty;
import static util.PreCondition.notNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


//jajudjaud
@Getter
@Entity
public final class Cliente {

    public static final int MAX_LENGTH_NOME = 100;
    
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
    private Integer id;
	private String nome;
	
	public Cliente() {}
	
	public Cliente(Builder builder) {
		super();
		this.nome = builder.nome;
	}
		
   public void update(String nome) {
	   checkObject(nome);

        this.nome = nome;
    }

   public static Builder getBuilder() {
       return new Builder();
   }
   
    @NoArgsConstructor
    public static class Builder {

        private String nome;
        
        public Builder nome(String nome) {
            this.nome = nome;
            return this;
        }

        public Cliente build() {
            Cliente build = new Cliente(this);

            build.checkObject(build.getNome());

            return build;
        }            
    }

    
    private void checkObject(String nome) {
        notNull(nome, "Nome não pode ser nulo");
        notEmpty(nome, "Nome não pode estar vazio");
        isTrue(nome.length() <= MAX_LENGTH_NOME,
                "Nome cannot be longer than %d characters",
                MAX_LENGTH_NOME
        );
    
    }
}
