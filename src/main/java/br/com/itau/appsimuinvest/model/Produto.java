package br.com.itau.appsimuinvest.model;

import static util.PreCondition.isTrue;
import static util.PreCondition.notEmpty;
import static util.PreCondition.notNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
public final class Produto {

	public static final int MAX_LENGTH_DESCRICAO_PRODUTO = 100;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Integer id; 
	private String descricaoProduto;
	private double taxaRedendimento;
	
	public Produto(){}
	
	public Produto(Builder builder){
		super();
		this.descricaoProduto = builder.descricaoProduto;
		this.taxaRedendimento = builder.taxaRedendimento;
	}
	
	public void update(String descricaoProduto, double taxaRedendimento){
		 checkObject(descricaoProduto, taxaRedendimento);
		 
		 this.descricaoProduto = descricaoProduto;
		 this.taxaRedendimento = taxaRedendimento;
	}
	 
	public static Builder getBuilder() {
	        return new Builder();
	}
		 
	@NoArgsConstructor
    public static class Builder {
		
		 private String descricaoProduto;
		 private double taxaRedendimento;
		
		 public Builder descricaoProduto(String descricaoProduto) {
	            this.descricaoProduto = descricaoProduto;
	            return this;
	     }
		 
		 public Builder taxaRedendimento(double taxaRedendimento) {
	            this.taxaRedendimento = taxaRedendimento;
	            return this;
	     }
		 
		 public Produto build(){
			 
			 Produto build = new Produto(this);
			 build.checkObject(build.getDescricaoProduto(), build.getTaxaRedendimento());
			 
			 return build;
		 }
		 
	}	 
	
	private void checkObject(String descricaoProduto, double taxaRedendimento) {
        notNull(descricaoProduto, "Nome não pode ser nulo");
        notEmpty(descricaoProduto, "Nome não pode estar vazio");
        isTrue(descricaoProduto.length() <= MAX_LENGTH_DESCRICAO_PRODUTO,
                "Nome cannot be longer than %d characters",
                MAX_LENGTH_DESCRICAO_PRODUTO
        );
        
        notNull(taxaRedendimento, "Taxa de rendimento não pode ser nulo");		      
    }
		 
	
}
