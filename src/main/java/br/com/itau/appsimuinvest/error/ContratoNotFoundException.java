package br.com.itau.appsimuinvest.error;

@SuppressWarnings("serial")
public class ContratoNotFoundException extends RuntimeException{

	public ContratoNotFoundException(Integer id) {
		super(String.format("Nenhuma Contrato encontrada pelo id: <%s>", id));
	}
}
