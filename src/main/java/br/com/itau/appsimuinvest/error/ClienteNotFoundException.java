package br.com.itau.appsimuinvest.error;

@SuppressWarnings("serial")
public class ClienteNotFoundException extends RuntimeException{

	public ClienteNotFoundException(Integer id){
		super(String.format("Nenhum Cliente encontrado pelo id: <%s>", id));
	}
}
