package br.com.itau.appsimuinvest.error;

@SuppressWarnings("serial")
public class ProdutoNotFoundException extends RuntimeException{

	public ProdutoNotFoundException(Integer id) {
		super(String.format("Nenhum Produto encontrado pelo id: <%s>", id));
	}
}
