package br.com.itau.appsimuinvest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppSimuinvestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppSimuinvestApplication.class, args);
	}

}
