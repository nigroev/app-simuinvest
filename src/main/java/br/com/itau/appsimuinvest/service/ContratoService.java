package br.com.itau.appsimuinvest.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.appsimuinvest.dto.ContratoDTO;
import br.com.itau.appsimuinvest.error.ContratoNotFoundException;
import br.com.itau.appsimuinvest.model.Contrato;
import br.com.itau.appsimuinvest.repository.ContratoRepository;
import br.com.itau.appsimuinvest.service.impl.ContratoServiceImpl;

@Service
public final class ContratoService implements ContratoServiceImpl{

	@Autowired
	private ContratoRepository repository;
	
	@Override
	public ContratoDTO create(ContratoDTO contrato) {
		Contrato persisted = Contrato.getBuilder()
				.cliente(contrato.getCliente())
				.produto(contrato.getProduto())
				.dataContratacao(contrato.getDataContratacao())
				.capitalInvestido(contrato.getCapitalInvestido())
				.build();
		persisted = repository.save(persisted);
		return convertToDTO(persisted);
	}

	@Override
	public ContratoDTO delete(Integer id) {
		Contrato deleted = findcontratoById(id);
		repository.delete(deleted);
		return convertToDTO(deleted);
	}

	@Override
	public List<ContratoDTO> findAll() {
		List<Contrato> contratoEntries = repository.findAll();
		return convertToDTOs(contratoEntries);
	}

	@Override
	public ContratoDTO findById(Integer id) {
		Contrato found = findcontratoById(id); 
		return convertToDTO(found);
	}

	@Override
	public ContratoDTO update(ContratoDTO contrato) {
		Contrato update = findcontratoById(contrato.getId());
		update.update(contrato.getCliente(), contrato.getProduto(), contrato.getDataContratacao(), contrato.getCapitalInvestido());
		update = repository.save(update);
		return convertToDTO(update);
	}

	private Contrato findcontratoById(Integer id) {
        Optional<Contrato> result = repository.findById(id);
        return result.orElseThrow(() -> new ContratoNotFoundException(id));
 
    }
	
	private List<ContratoDTO> convertToDTOs(List<Contrato> models) {
        return models.stream()
                .map(this::convertToDTO)
                .collect(toList());
    }
	
	private ContratoDTO convertToDTO(Contrato model) {
		ContratoDTO dto = new ContratoDTO();
        dto.setId(model.getId());
        dto.setCliente(model.getCliente());
        dto.setProduto(model.getProduto());
        dto.setDataContratacao(model.getDataContratacao());
        dto.setCapitalInvestido(model.getCapitalInvestido());
        return dto;
    }
}
