package br.com.itau.appsimuinvest.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.appsimuinvest.dto.ProdutoDTO;
import br.com.itau.appsimuinvest.error.ClienteNotFoundException;
import br.com.itau.appsimuinvest.model.Produto;
import br.com.itau.appsimuinvest.repository.ProdutoRepository;
import br.com.itau.appsimuinvest.service.impl.ProdutoServiceImpl;

@Service
public final class ProdutoService implements ProdutoServiceImpl {

	@Autowired
	private ProdutoRepository repository;
	
	@Override
	public ProdutoDTO create(ProdutoDTO produto) {
		Produto persisted = Produto.getBuilder()
                .descricaoProduto(produto.getDescricaoProduto())                
                .taxaRedendimento(produto.getTaxaRedendimento())
                .build();
        persisted = repository.save(persisted);
        return convertToDTO(persisted);
	}

	@Override
	public ProdutoDTO delete(Integer id) {
		Produto deleted = findProdutoById(id);
        repository.delete(deleted);
        return convertToDTO(deleted);
	}

	@Override
	public List<ProdutoDTO> findAll() {
		List<Produto> clienteEntries = repository.findAll();
        return convertToDTOs(clienteEntries);
	}

	@Override
	public ProdutoDTO findById(Integer id) {
		Produto found = findProdutoById(id);
	    return convertToDTO(found);
	}

	@Override
	public ProdutoDTO update(ProdutoDTO produto) {
		 Produto updated = findProdutoById(produto.getId());
	     updated.update(produto.getDescricaoProduto(), produto.getTaxaRedendimento());
	     updated = repository.save(updated);
	     return convertToDTO(updated);
	}
	
	private Produto findProdutoById(Integer id) {
        Optional<Produto> result = repository.findById(id);
        return result.orElseThrow(() -> new ClienteNotFoundException(id));
 
    }
	
	private List<ProdutoDTO> convertToDTOs(List<Produto> models) {
        return models.stream()
                .map(this::convertToDTO)
                .collect(toList());
    }
	
	private ProdutoDTO convertToDTO(Produto model) {
		ProdutoDTO dto = new ProdutoDTO();
		dto.setId(model.getId());
        dto.setDescricaoProduto(model.getDescricaoProduto());
        dto.setTaxaRedendimento(model.getTaxaRedendimento());
        return dto;
    }

}
