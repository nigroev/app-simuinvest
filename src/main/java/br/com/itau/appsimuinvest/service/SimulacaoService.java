package br.com.itau.appsimuinvest.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import br.com.itau.appsimuinvest.dto.OfertaDTO;
import br.com.itau.appsimuinvest.dto.SimulacaoDTO;

@Service
public final class SimulacaoService {

	public OfertaDTO simular(SimulacaoDTO simulacao){
		
		OfertaDTO found = new OfertaDTO();
		found.setCliente(simulacao.getCliente());
		found.setCapitalInvestido(simulacao.getCapitalInvestido());
		found.setProduto(simulacao.getProduto());
		found.setQuantidadeMeses(simulacao.getQuantidadeMeses());
		
		BigDecimal capitalAcumulado = simulacao.getCapitalInvestido();
		
		for (int i = 0; i < simulacao.getQuantidadeMeses(); i++) {
			
			capitalAcumulado = capitalAcumulado.add(capitalAcumulado.multiply(BigDecimal.valueOf(simulacao.getProduto().getTaxaRedendimento())));
			found.setCapitalFuturo(capitalAcumulado);
		}
	
		return found;
	}
	
}
