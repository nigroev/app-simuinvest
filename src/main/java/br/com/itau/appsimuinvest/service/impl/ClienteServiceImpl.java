package br.com.itau.appsimuinvest.service.impl;

import java.util.List;



import br.com.itau.appsimuinvest.dto.ClienteDTO;


public interface  ClienteServiceImpl {

	ClienteDTO create(ClienteDTO cliente);
	
	ClienteDTO delete(Integer id);
	
	List<ClienteDTO> findAll();
	
	ClienteDTO findById(Integer id);
	 
	ClienteDTO update(ClienteDTO cliente);
}
