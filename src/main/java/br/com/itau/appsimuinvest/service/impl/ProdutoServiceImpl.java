package br.com.itau.appsimuinvest.service.impl;

import java.util.List;

import br.com.itau.appsimuinvest.dto.ProdutoDTO;


public interface ProdutoServiceImpl {

	ProdutoDTO create(ProdutoDTO produto);
	
	ProdutoDTO delete(Integer id);
	
	List<ProdutoDTO> findAll();
	
	ProdutoDTO findById(Integer id);
	
	ProdutoDTO update (ProdutoDTO produtoDTO);
}
