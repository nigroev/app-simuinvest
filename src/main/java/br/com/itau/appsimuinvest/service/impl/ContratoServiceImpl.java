package br.com.itau.appsimuinvest.service.impl;

import java.util.List;

import br.com.itau.appsimuinvest.dto.ContratoDTO;

public interface ContratoServiceImpl {

	ContratoDTO create(ContratoDTO contrato);
	
	ContratoDTO delete(Integer id);
	
	List<ContratoDTO> findAll();
	
	ContratoDTO findById(Integer id);
	
	ContratoDTO update(ContratoDTO contrato);
}
