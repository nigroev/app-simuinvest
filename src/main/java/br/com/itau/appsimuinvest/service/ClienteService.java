package br.com.itau.appsimuinvest.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.itau.appsimuinvest.dto.ClienteDTO;
import br.com.itau.appsimuinvest.error.ClienteNotFoundException;
import br.com.itau.appsimuinvest.model.Cliente;
import br.com.itau.appsimuinvest.repository.ClienteRepository;
import br.com.itau.appsimuinvest.service.impl.ClienteServiceImpl;

@Service
public final class ClienteService implements ClienteServiceImpl{

	@Autowired
	private ClienteRepository repository;
	
	
	@Override
	public ClienteDTO create(ClienteDTO cliente) {
		Cliente persisted = Cliente.getBuilder()
                .nome(cliente.getNome())                
                .build();
        persisted = repository.save(persisted);
        return convertToDTO(persisted);
	}

	@Override
	public ClienteDTO delete(Integer id) {
		Cliente deleted = findClienteById(id);
        repository.delete(deleted);
        return convertToDTO(deleted);
	}

	@Override
	public List<ClienteDTO> findAll() {
		List<Cliente> clienteEntries = repository.findAll();
        return convertToDTOs(clienteEntries);
	}

	@Override
	public ClienteDTO findById(Integer id) {
		Cliente found = findClienteById(id);
	    return convertToDTO(found);
	}

	@Override
	public ClienteDTO update(ClienteDTO cliente) {
		 Cliente updated = findClienteById(cliente.getId());
	     updated.update(cliente.getNome());
	     updated = repository.save(updated);
	     return convertToDTO(updated);
	}
	
	private Cliente findClienteById(Integer id) {
        Optional<Cliente> result = repository.findById(id);
        return result.orElseThrow(() -> new ClienteNotFoundException(id));
 
    }
	
	private List<ClienteDTO> convertToDTOs(List<Cliente> models) {
        return models.stream()
                .map(this::convertToDTO)
                .collect(toList());
    }
	
	private ClienteDTO convertToDTO(Cliente model) {
        ClienteDTO dto = new ClienteDTO();
        dto.setId(model.getId());
        dto.setNome(model.getNome());
        
        return dto;
    }

}
