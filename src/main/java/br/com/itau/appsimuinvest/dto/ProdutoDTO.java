package br.com.itau.appsimuinvest.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import br.com.itau.appsimuinvest.model.Produto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoDTO {

	private Integer id; 
	
	@NotEmpty
    @Size(max = Produto.MAX_LENGTH_DESCRICAO_PRODUTO)
	private String descricaoProduto;
	
	@NotNull
	private double taxaRedendimento;
}
