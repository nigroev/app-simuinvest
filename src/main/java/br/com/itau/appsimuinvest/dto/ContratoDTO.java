package br.com.itau.appsimuinvest.dto;

import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import br.com.itau.appsimuinvest.model.Cliente;
import br.com.itau.appsimuinvest.model.Produto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratoDTO {

	private Integer id;
	
	@NotNull
	private Cliente cliente;
	
	@NotNull
	private Produto produto;
	
	@NotNull
	private Date dataContratacao;
	
	@NotNull
	@DecimalMin(value = "50.00")
	private BigDecimal capitalInvestido;
	
}
;