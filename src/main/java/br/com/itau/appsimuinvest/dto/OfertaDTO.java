package br.com.itau.appsimuinvest.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;

import br.com.itau.appsimuinvest.model.Cliente;
import br.com.itau.appsimuinvest.model.Produto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfertaDTO {

	@NotNull
	private Cliente cliente;
	
	@NotNull
	private Produto produto;
	
	@NotNull
	private BigDecimal capitalInvestido;
	
	@NotNull
	@Min(value = 1)
	private int quantidadeMeses;
	
	@NotNull
	@NumberFormat(pattern = "#,###,###,###.##")
	private BigDecimal capitalFuturo;
	
}
