package br.com.itau.appsimuinvest.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import br.com.itau.appsimuinvest.model.Cliente;
import br.com.itau.appsimuinvest.model.Produto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimulacaoDTO {

	@NotNull
	private Cliente cliente;
	
	@NotNull
	private Produto produto;
	
	@NotNull
	private BigDecimal capitalInvestido;
	
	@NotNull
	@Min(value = 1)
	private int quantidadeMeses;
	
	
}
