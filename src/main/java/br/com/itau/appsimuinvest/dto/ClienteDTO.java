package br.com.itau.appsimuinvest.dto;



import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import br.com.itau.appsimuinvest.model.Cliente;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO {

	private Integer id;
	
	@NotEmpty
    @Size(max = Cliente.MAX_LENGTH_NOME)
    private String nome;
	
}
