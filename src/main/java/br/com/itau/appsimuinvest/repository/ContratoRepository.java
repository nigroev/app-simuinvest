package br.com.itau.appsimuinvest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.itau.appsimuinvest.model.Contrato;

public interface ContratoRepository extends JpaRepository<Contrato, Integer>{

}
