package br.com.itau.appsimuinvest.repository;



import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;
import br.com.itau.appsimuinvest.model.Produto;


public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
