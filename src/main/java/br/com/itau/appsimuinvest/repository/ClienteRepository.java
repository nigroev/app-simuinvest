package br.com.itau.appsimuinvest.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import br.com.itau.appsimuinvest.model.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente , Integer>{

	
}
